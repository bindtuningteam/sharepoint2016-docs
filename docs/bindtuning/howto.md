### How to Install

BindTuning provides you with the flexibility to install our products the way you want to, regardless of technical knowledge or experience.

Following this, BindTuning offers two main installation processes:

- **Automated** installation;
- **Manual** installation.

**Note:** We recommend the **Automated** installation, due to its easy deployment process.

---
### Automated Installation

The **Automated** installation process makes the deployment of BindTuning's products as easy as a simple click. 

#### Provisioning Engine desktop app

The **Provisioning Engine** will also automatically deploy your Products, to both SharePoint Online and SharePoint on-premises.

<a href="https://bindtuning-sharepoint-2016-themes.readthedocs.io/en/latest/automated/provisioning/installation/">Here</a> you'll learn how to install the products using the Provisioning desktop app.

---
### Manual Installation

The Manual installation process allows for more granular control on the installation with an added complexity layer, not present in the Automated installation.

The Manual installation for SharePoint 2016 can be divided in two big categories:

- **Farm** solution (installation instructions can be found <a href="https://bindtuning-sharepoint-2016-themes.readthedocs.io/en/latest/manual/farm/requirements/">here</a>);
- **Sandbox** solution (installation instructions can be found <a href="https://bindtuning-sharepoint-2016-themes.readthedocs.io/en/latest/manual/sandbox/requirements/">here</a>);



