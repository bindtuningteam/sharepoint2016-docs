The themes are provided in 2 different packaging all following Microsoft guidelines:

 - ***Farm Solution***, folder for Farm environments
 - ***Sandbox Solution***, for Sandbox environments.

After unzipping your Theme package, you will find two folders, a **Farm Solution** and **Sandbox Solution**. 
Depending on the SharePoint configuration you may install all the packages. 

![zip.png](../images/zip.png)


-------------

**Farm Solution** (Classic SharePoint) 📁

── res 📁

└── Eula.rtf <br>
└── yourthemename.SP2016.wsp<br>
└── logo.png<br>
└── start.png<br>

── Install.yourthemenamePackage.ps1<br>
── Setup.exe<br>
── Setup.exe.config<br>

-------------

**Sandbox Solution** (Classic SharePoint) 📁

── yourthemename.SP2016.wsp