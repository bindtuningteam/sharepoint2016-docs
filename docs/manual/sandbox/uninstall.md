
### Reset the theme Master pages 

The first step in uninstalling any BindTuning theme for the Sandbox solution, is to reset the theme master pages and switch to one of SharePoint's default master pages **(seattle or oslo)** before uninstalling the theme. <br>
This way, your site will keep on displaying your content and prevent any possible error.

1. On your **root site**, open the **Settings** menu and click on **Site Settings**;

2. Under **Look and Feel**, click on *Master page*; 


	**Note:** If you haven't activated SharePoint Publishing Features the Master page option will not appear on the list. Further details on how to activate SharePoint Publishing Features can be found [here](https://bindtuning-sharepoint-2016-themes.readthedocs.io/en/latest/manual/sandbox/requirements).
<br>

3. With the option "Specify a master page to be used..." selected, pick one of SharePoint's default master pages **(seattle or oslo)** for both the *Site Master Page* and the *System Master Page*; 

4. Check the option **"Reset all subsites..."** for both the *Site Master Page* and the *System Master Page*, in case any of your subsites is still using any BindTuning master page; 

5. Click **OK**.

Done! Lets move on to deactivating and deleting your theme. ✅

---
### Deactivate and delete the theme on SharePoint

1. On your root site open the **Settings** menu and click on **Site Settings**;

2. Under *Web Design Galleries*, click on **Solutions**;

3. Select *yourthemename.SP2016.wsp* file, and click on **Deactivate**;

4. Select *yourthemename.SP2016.wsp* file again, and click on **Delete**.

---
### Finishing off

To completly remove and uninstall your theme, follow the below-described steps:

1. Open your site with **SharePoint Designer** and, on the left column, click on **All Files**;
2. Open **_catalogs** and click on **masterpage**;
3. Delete the folder *yourthemename*;
4. Open *All Files* and click on **Style Library**;
5. Delete the folder *yourthemename*. 

You've now successfully removed and uninstalled your BindTuning theme. ✅