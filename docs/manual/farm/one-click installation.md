This is the easiest way to install your theme. With the one-click installation tool, you can install a theme in a few clicks, without having to spend a whole lot of time setting everything up. 

Let's get right to it! 

1. Inside your theme's package, open the **Farm Solution** folder;
2. Click twice on the **Setup.exe** file to open the one-click installer;
3. With the one-click tool open, click **Next**;

	**Note:** The installer will now check if all the requirements are met, in order for the theme to be installed. If any of the requirements fail, click **Abort**, fix the failed requirements and re-run the one-click installer. Once done, you will see a success message.
<br>
4. Next, you will see our licensing agreement. When you're done, check **"Accept the licensing terms and conditions"** and click **Next**; 
	
Your theme is now being installed! 🕐

After the installation, click **Close** to close the One-click Installer. <br> If you'd like to check the installation log, simply click on **Next**.


---

### Check if the Theme was successfully installed

Before moving on to activating the theme, let's confirm if the theme was successfully installed.

1. Access your **SharePoint Central Administration**;
2. Click on **System Settings** and open **Manage Farm Solutions**;

	**Note:** You can also enter your Solutions Management with this path: **http://[central_administration_url]/_admin/Solutions.aspx**.

If the installation was successful, under the column **Status** you'll see the value **Deployed** and, under **Deployed To** the value **Globally deployed**.

![](https://bitbucket.org/repo/g6RLX7/images/3762309561-sp_2013_farm_installation.png)

Theme installed! ✅ 
___

### Apply the theme  
After confirming the theme was successfully installed, it has to be activated:

1. Click on **Settings** ⚙️ and then **Site settings**;
2. Under **Site Actions**, click on **Manage Site Features**;
3. Find your theme and click on **Activate**.

Theme applied! ✅

---
### Set the master page 
The final step consists in changing your current master page to one of the theme's master page.

1. Click on **Settings** ⚙️ and then **Site Settings**;  
2. Under **Look and Feel**, click on **Master page**;
	
	![changethemasterpage_1](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/changethemasterpage_1.png) 
 
3. Choose the master page you want to apply. The master pages included on your theme start with *yourthemename*;

	![changethemasterpage_3](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/changethemasterpage_3.png) 

4. Click **Ok**. 

Master Page set! ✅



