### One-click Uninstall

The easiest way to uninstall your BindTuning theme from your SharePoint site is with BindTuning's One-click tool.

To uninstall your theme using the One-click tool follow the steps below:

1. Inside the **FarmSolution** folder, click twice on the **Setup.exe** file to open the One-click tool;
2. Click **Next**;
3.  **The installer will now check if the theme can be removed**.  Once done, you will see a success message. If any of the requirements fail, click on **Abort**, fix the failed requirements and re-run the executable;
4.  Click **Next**;
5.  Select **Remove** and click **Next**;
6.  **Check if your theme appears on the list** (the theme will only appear if it is activated on your site) and click **Next**; 
7.  **The theme will be removed** and the **seattle master page** will be applied. Once done, you will see a success message. 
8.  Click **Next** to check the log or **Close** to exit the installer. 

The theme was successfully removed from your site. ✅

---

### Manual Uninstall

**Note:** Go through these steps if you want to do a **full manual uninstallation** of your BindTuning theme. 
<br>

#### Reset the theme master pages

If you are using any of the theme master pages, you will need to reset them and switch to one of SharePoint's default master pages (**seattle** or **oslo**) before moving on to deactivating and uninstalling the theme.

1. On your root site open the **Settings** menu and click on **Site Settings**;
2. Under *Look and Feel*, click on **Master page**;
3. With the option "Specify a master page to be used..." selected, pick one of SharePoint's default master pages **(seattle or oslo)** for both the **Site Master Page** and the **System Master Page**; 
4. Also check the option "Reset all subsites..."** for both the **Site Master Page** and the **System Master Page**; 
5. Click **OK.**

#### Deactivate the theme

If the BindTuning theme is still active, you will have to deactivate it before proceeding with the uninstallation. 

1. On your root site open the **Settings** menu and click on **Site Settings**;
2. Under **Site Actions**, click on **Manage site features**;
3. Search for ***yourthemename* Theme Package** and click on **Deactivate**.

#### Uninstall the theme

**Important:** Before uninstalling the theme check if the **Execution Policy** is set to **Unrestricted\Bypass**. <a target="_blank" href="https://bindtuning-sharepoint-2016-themes.readthedocs.io/en/latest/manual/farm/requirements/">Here</a> you can find the instructions on how to set the Execution Policy. 

Inside the Farm Solution folder you will find the script ***Install.yourthemenamePackage.ps1***. This is the script you will use to remove the theme.

1. **Open SharePoint Management Shell** and run it as **Admin**;
2. On the console **go to the path where the script is**;
3. Insert the command `.\Install.yourthemenamePackage.ps1` 
4. **Accept** the *Execution Policy Change* (if it appears);
4. **Chose option 2** to retract the theme and **hit enter**; 

	**Note:** If you try to access your site during the process you will see a 503 status error code.

6. Insert the command again `.\Install.yourthemenamePackage.ps1` 
7. **Chose option 3** to remove the theme from your site and **hit enter**. 

The theme was successfully removed from your website. ✅


**Note:** You can also use the central administration
(http://[central_administration_url]/_admin/Solutions.aspx) to remove your theme from your site. Just click on the theme, retract and the delete it from the Farm.