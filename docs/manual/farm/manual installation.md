1. Open your **SharePoint Management Shell** as an **admin**;
1. On the console, go to the path where the **Install.yourthemenamePackage.ps1** script is. <br>
**Note:** It's usually located in **C:/Downloads/"yourthemename".SP2016/FarmSolution**;
	
	![spf_installation_8](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/spf_installation_8.png)
	 
1. Once inside the folder, you can just insert the command `.\Install.yourthemenamePackage.ps1`. If the **Execution Policy Change** appears, accept it;
1. At this point different options will appear. Choose **Option 1** to install the theme and **hit enter**; 
1. Your theme is now being installed! 

Once this is done you will see a message letting you know the theme was successfully installed. ✅

___

### Check if the Theme was installed

Before moving on to activating the theme, let's confirm if the theme was successfully installed.

![spf_installation_10](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/spf_installation_10.png) 

1. Access your **SharePoint Central Administration**;
2. Click on **System Settings** and open **Manage Farm Solutions**. <br>
You can also enter your **Solutions Management** with this path: **http://[central_administration_url]/_admin/Solutions.aspx**

If the theme was successfully installed, the **Status** tab should display **Deployed**, and the **Deployed to** tab should display *Globally Deployed*. 

Theme installed! ✅

___

### Activate the theme  
After confirming the theme was successfully installed, it has to be activated:

1. Click on **Settings** ⚙️ and then **Site settings**;
2. Under **Site Actions**, click on **Manage Site Features**;
3. Select your theme and click on **Activate**.

Theme activated! ✅

___

### Set the master page 
The final step is to change your current master page to one of the theme's master page.

1. Click on **Settings** ⚙️ and then **Site settings**;
2. Under **Look and Feel**, click on **Master page**;
	
	![changethemasterpage_1](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/changethemasterpage_1.png) 
 
3. Choose the master page you want to apply. Your theme's master pages starts by *yourthemename*;

	![changethemasterpage_3](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/changethemasterpage_3.png) 
4. Click **Ok**. 

Master Page set! ✅