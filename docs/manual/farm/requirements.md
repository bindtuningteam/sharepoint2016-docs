Before moving on to installing your theme, there is a couple of things you will need to do. To activate some features you will have to be an **Admin** or have been granted permissions from your Admin.

- Activate the **SharePoint Server Publishing Infrastructure** feature.
- Deactivate the **Minimal Download Strategy** feature, that usually appears activated by default in these type of sites.
- Activate the **SharePoint Server Publishing** feature.

Let's start by activating the **SharePoint Server Publishing Infrastructure** feature: 

1. Click on **Settings** ⚙️ and proceed to click on **Site settings**;
	
	![](https://bitbucket.org/repo/g6RLX7/images/3576738087-sp_2013_farm_installation_1.png)
	 
2. Under **Site Collection Administration**, click on **Site collection features**; 

	![](https://bitbucket.org/repo/g6RLX7/images/2999141458-sp_2013_farm_installation_2.png)
	
	**Note:** If you're seeing **Go to top level site settings** instead of **Site collection features**, under **Site Collection Administration**, click on **Go to top level site settings** and then **Site collection features**.

3. Search for *SharePoint Server Publishing Infrastructure* and click on **Activate**; 
	
	![](https://bitbucket.org/repo/g6RLX7/images/2238078432-sp_2013_farm_installation_4.png)
	
4. Go to your root site and click **Settings** ⚙️ and then **Site settings**;

5. Under **Site Actions**, click on **Manage Site Features**;

	![](https://bitbucket.org/repo/g6RLX7/images/593606482-sp_2013_farm_installation_3.png)

6. Search for *Minimal Download Strategy* and click on **Deactivate**. 

7. On the same page, search for *SharePoint Server Publishing* and click on **Activate**.

All the required **Publishing Features** have been activated. ✅

---

### Get the server ready 

**Note:** These modifications are only required for the **Manual installation** of the theme.

Before installing and activating the theme, you will need to do a quick server set up to make sure your server is ready to process PowerShell scripts.

You will need to **change the Execution Policy from Restricted to Unrestricted\bypass**. For more information, read Microsoft's article on <a href="https://technet.microsoft.com/en-us/library/hh847748.aspx" target="_blank">Execution Policies</a>.

<p><strong>Note:</strong> If your Execution Policy is already set to <b>Unrestricted</b>, move on to the <b><a href="https://bindtuning-sharepoint-2016-themes.readthedocs.io/en/latest/manual/farm/manual%20installation/" target="_blank">Theme Installation</a></b>.</p>

1. Open **SharePoint Management Shell** as **Admin**;
2. Enter the command `Get-ExecutionPolicy`. <br> 
You should get a **RemoteSigned** message;
3. Insert the command `Set-ExecutionPolicy Unrestricted`; 
4. After reading the Execution Policy Change, type **Y** and hit enter.
	
	![](https://bitbucket.org/repo/g6RLX7/images/1999946959-executionpolicychange.png)

Now that the server is ready to run PowerShell scripts you can move on to installing the theme. ✅