Ready to install your Theme? First, you need to download.

1. Access your account at <a href="https://bindtuning.com" target="_blank">BindTuning</a>;
2. Go to **Design** and switch to the **My Themes** tab.
3. Mouse hover the theme and click **More Details** to open the Theme details page;

    ![theme-select.png](..\images\select-theme.png)

4. Last but not least, click on **Download**.

    ![download-theme](..\images\download-theme.png)